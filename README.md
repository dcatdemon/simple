simple
============

This is to be a collection of puppet modules to do simple tasks like user management.

Maybe I would rename it in the future, but this is just for reference and hope to build a collection of small tasks.

## Modules
simple::users

## Profiles
simple

## Usage
In hiera, 

For example, in yaml:
       
	---
	simple::users::myusers:
		testuser:
    		ensure: present
    		uid: 10001
    		password: verySecretPassword
    		home: /home/testuser
    		shell: /bin/bash
	simple::users::mygroups:
		testuser:
			ensure: present
			gid: 10001
       
