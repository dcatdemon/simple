class simple::users (
  $myusers,
  $mygroups,
)
{
  create_resources(user, $myusers)
  create_resources(group, $mygroups)
}
